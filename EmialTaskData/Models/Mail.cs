﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.RegularExpressions;
using EmialTaskData.Const;

namespace EmialTaskData.Models
{
    public class Mail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(450)]
        [Required]
        [RegularExpression(RegularExpression.EmialRegex)]
        public string Emial { get; set; }

        public int? MailGroupId { get; set; }

        public MailGroup? MailGroupNavigation { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }
    }
}
