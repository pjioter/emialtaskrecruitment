﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmialTaskData.Models
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        [StringLength(450)]
        public string FirstName { get; set; }
        
        [StringLength(450)]
        public string LastName { get; set; }

        [StringLength(450)]
        [Required]
        public string Username { get; set; }

        public byte[] PasswordHash { get; set; }

        public byte[] PasswordSalt { get; set; }


        public virtual ICollection<MailGroup> MailGroupNavigation { get; set; }

        public virtual ICollection<Mail> MailNavigation { get; set; }


    }
}
