﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmialTaskData.Models
{
    public class MailGroup
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(450)]
        public string GroupName { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public virtual ICollection<Mail> MailNavigation { get; set; }

    }
}
