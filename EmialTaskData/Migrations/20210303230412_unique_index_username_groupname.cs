﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmialTaskData.Migrations
{
    public partial class unique_index_username_groupname : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Users_Username",
                table: "Users",
                column: "Username",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MailGroups_GroupName",
                table: "MailGroups",
                column: "GroupName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Users_Username",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_MailGroups_GroupName",
                table: "MailGroups");
        }
    }
}
