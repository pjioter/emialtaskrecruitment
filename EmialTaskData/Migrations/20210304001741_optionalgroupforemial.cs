﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmialTaskData.Migrations
{
    public partial class optionalgroupforemial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Mails_MailGroups_MailGroupId",
                table: "Mails");

            migrationBuilder.AlterColumn<int>(
                name: "MailGroupId",
                table: "Mails",
                type: "INTEGER",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AddForeignKey(
                name: "FK_Mails_MailGroups_MailGroupId",
                table: "Mails",
                column: "MailGroupId",
                principalTable: "MailGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Mails_MailGroups_MailGroupId",
                table: "Mails");

            migrationBuilder.AlterColumn<int>(
                name: "MailGroupId",
                table: "Mails",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Mails_MailGroups_MailGroupId",
                table: "Mails",
                column: "MailGroupId",
                principalTable: "MailGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
