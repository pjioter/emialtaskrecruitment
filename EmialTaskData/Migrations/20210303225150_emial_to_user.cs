﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EmialTaskData.Migrations
{
    public partial class emial_to_user : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Mail_MailGroup_MailGroupId",
                table: "Mail");

            migrationBuilder.DropForeignKey(
                name: "FK_MailGroup_Users_UserId",
                table: "MailGroup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MailGroup",
                table: "MailGroup");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Mail",
                table: "Mail");

            migrationBuilder.RenameTable(
                name: "MailGroup",
                newName: "MailGroups");

            migrationBuilder.RenameTable(
                name: "Mail",
                newName: "Mails");

            migrationBuilder.RenameIndex(
                name: "IX_MailGroup_UserId",
                table: "MailGroups",
                newName: "IX_MailGroups_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Mail_MailGroupId",
                table: "Mails",
                newName: "IX_Mails_MailGroupId");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Mails",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_MailGroups",
                table: "MailGroups",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Mails",
                table: "Mails",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Mails_UserId",
                table: "Mails",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_MailGroups_Users_UserId",
                table: "MailGroups",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Mails_MailGroups_MailGroupId",
                table: "Mails",
                column: "MailGroupId",
                principalTable: "MailGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Mails_Users_UserId",
                table: "Mails",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MailGroups_Users_UserId",
                table: "MailGroups");

            migrationBuilder.DropForeignKey(
                name: "FK_Mails_MailGroups_MailGroupId",
                table: "Mails");

            migrationBuilder.DropForeignKey(
                name: "FK_Mails_Users_UserId",
                table: "Mails");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Mails",
                table: "Mails");

            migrationBuilder.DropIndex(
                name: "IX_Mails_UserId",
                table: "Mails");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MailGroups",
                table: "MailGroups");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Mails");

            migrationBuilder.RenameTable(
                name: "Mails",
                newName: "Mail");

            migrationBuilder.RenameTable(
                name: "MailGroups",
                newName: "MailGroup");

            migrationBuilder.RenameIndex(
                name: "IX_Mails_MailGroupId",
                table: "Mail",
                newName: "IX_Mail_MailGroupId");

            migrationBuilder.RenameIndex(
                name: "IX_MailGroups_UserId",
                table: "MailGroup",
                newName: "IX_MailGroup_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Mail",
                table: "Mail",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MailGroup",
                table: "MailGroup",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Mail_MailGroup_MailGroupId",
                table: "Mail",
                column: "MailGroupId",
                principalTable: "MailGroup",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MailGroup_Users_UserId",
                table: "MailGroup",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
