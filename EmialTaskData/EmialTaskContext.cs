﻿using EmialTaskData.Models;
using Microsoft.EntityFrameworkCore;

namespace EmialTaskData
{
    public class EmialTaskContext : DbContext
    {
        public EmialTaskContext(DbContextOptions<EmialTaskContext> options)
           : base(options)
        {
        }

        public EmialTaskContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite("Data Source=EmialTaskDb.db");

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User>()
                .HasIndex(u => u.Username)
                .IsUnique();
            
            builder.Entity<MailGroup>()
                .HasIndex(u => u.GroupName)
                .IsUnique();
        }

        public DbSet<User> Users { get; set; }

        public DbSet<MailGroup> MailGroups { get; set; }

        public DbSet<Mail> Mails { get; set; }
    }
}
