﻿namespace EmialTaskData.Const
{
    public static class RegularExpression
    {
        public const string EmialRegex = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";
    }
}