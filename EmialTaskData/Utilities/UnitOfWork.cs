﻿using System;

namespace EmialTaskData.Utilities
{
    public class UnitOfWork : IDisposable
    {
        private readonly EmialTaskContext _context;

        public UnitOfWork(EmialTaskContext context)
        {
            _context = context;
        }
      
        public GenericRepository<T1> GetRepository<T1>()
           where T1 : class
        {
            return new GenericRepository<T1>(_context);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
