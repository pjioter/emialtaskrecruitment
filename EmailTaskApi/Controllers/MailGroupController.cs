﻿using EmialTaskBll.Service.MailGroupService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using AutoMapper;
using EmialTaskBll.CustomException;
using EmialTaskBll.Service.MailGroupService.Dto;
using EmialTaskData.Models;
using Microsoft.Data.Sqlite;


namespace EmailTaskApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class MailGroupController : ControllerBase
    {
        private readonly IMailGroupService _mailGroupService;
        private readonly IMapper _mapper;

        public MailGroupController(IMailGroupService mailGroupService, IMapper mapper)
        {
            _mailGroupService = mailGroupService;
            _mapper = mapper;
        }


        /// <summary>
        /// Get all Emial Groups for logged user.
        /// </summary>
        /// <returns>List of MailGroupModel by user</returns>
        /// <response code="200">Return all Emial Group for current logged user</response>
        /// <response code="400">If any unhandled exception</response>
        /// <response code="401">If user is not authorized</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public ActionResult<IEnumerable<MailGroupModel>> Get()
        {
            try
            {
                return Ok(_mailGroupService.Get());
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Get Emial Group for logged user by group id.
        /// </summary>
        /// <returns>Emial Group</returns>
        /// <response code="200">Returns EmialGroup by id for logged user</response>
        /// <response code="404">If group not founded</response>
        /// <response code="400">If any unhandled exception</response>
        /// <response code="401">If user is not authorized</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public ActionResult<MailGroupModel> Get(int id)
        {
            try
            {
                var mailGroup = _mailGroupService.Get(id);
                return Ok(mailGroup);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Get all emial from group by given id.
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns>Emial List from group</returns>
        /// <response code="200">Returns Emial List from group by id for logged user</response>
        /// <response code="404">If group not founded</response>
        /// <response code="400">If any unhandled exception</response>
        /// <response code="401">If user is not authorized</response>
        [HttpGet("mailFromGroup")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public ActionResult<GroupWithMails> MailFromGroup(int groupId)
        {
            try
            {
                var mailGroup = _mailGroupService.GetAllEmialFromGroup(groupId);
                return Ok(mailGroup);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Create a new mail group for logged user
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /createMailGroup
        ///     {
        ///         "groupName": "workers"
        ///     }
        ///
        /// </remarks>
        /// <param name="mailGroupModel"></param>
        /// <returns>New Created Group with group id and creator id</returns>
        /// <response code="201">Return the new created mail group with user id</response>
        /// <response code="400">If any unhandled exception</response>
        [HttpPost("createMailGroup")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<MailGroupModel> Post([FromBody] NewMailGroupModel mailGroupModel)
        {
            try
            {
                var groupModel = _mapper.Map<MailGroup>(mailGroupModel);
                var createdGroup = _mailGroupService.Create(groupModel);

                return CreatedAtAction(nameof(Get), new {id = createdGroup.Id}, createdGroup);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
            
        }


        /// <summary>
        /// Create a new mail group for logged user
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT
        ///     {   "id":       1,
        ///         "name":     "test",
        ///         "userId":   2,
        ///     }
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="mailGroupModel"></param>
        /// <returns>New Created Group with group id and creator id</returns>
        /// <response code="201">Return the new created mail group with user id</response>
        /// <response code="400">If any unhandled exception</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<MailGroupModel> Put(int id, [FromBody] MailGroupModel mailGroupModel)
        {
            if (id != mailGroupModel.Id)
            {
                return BadRequest();
            }

            try
            {
                var updatedModel = _mailGroupService.Update(mailGroupModel);
                if (updatedModel is null)
                {
                    return NotFound();
                }

                return NoContent();
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        /// <summary>
        /// Deletes a specific Emial Group for logged user.
        /// </summary>
        /// <param name="id"></param>
        /// <response code="204">If group deleted</response>
        /// <response code="404">If group for delete not founded</response>
        /// <response code="400">If any unhandled exception</response> 
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Delete(int id)
        {
            try
            {
                _mailGroupService.Delete(id);
                return NoContent();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
