﻿using AutoMapper;
using EmialTaskBll.Service.UserService;
using EmialTaskBll.Service.UserService.Dto;
using EmialTaskData.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using EmialTaskBll.Helpers;

namespace EmailTaskApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private IMapper _mapper;

        public UsersController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        /// <summary>
        /// Authenticate a User.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /authenticate
        ///     {
        ///         "username": "czart",
        ///          "password": "qazwsx"
        ///     }
        ///
        /// </remarks>
        /// <param name="authenticateModel"></param>
        /// <returns>Authenticated user</returns>
        /// <response code="200">Returns the authenticated user</response>
        /// <response code="400">If authentication fails</response>   
        [AllowAnonymous]
        [HttpPost("authenticate")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]       
        public async Task<IActionResult> Authenticate([FromBody] AuthenticateModel model)
        {
            var user = _userService.Authenticate(model.Username, model.Password).Result;

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user.WithoutPassword());
        }

        /// <summary>
        /// Register a new User.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /register
        ///     {
        ///         "firstName": "Janusz",
        ///         "lastName": "Tracz",
        ///         "username": "czart",
        ///         "password": "qazwsx"
        ///     }
        ///
        /// </remarks>
        /// <param name="registerModel"></param>
        /// <returns>Authenticated user</returns>
        /// <response code="204">Returns OK If user succesfuly registered</response>
        /// <response code="400">If registration fails</response>   
        [AllowAnonymous]
        [HttpPost("register")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Register([FromBody] RegisterModel model)
        {
            var user = _mapper.Map<User>(model);

            try
            {
                _userService.Create(user, model.Password);
                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
