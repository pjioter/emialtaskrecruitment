﻿using EmialTaskBll.Service.MailService.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using AutoMapper;
using EmialTaskBll.CustomException;
using EmialTaskBll.Service.MailGroupService.Dto;
using EmialTaskBll.Service.MailService;
using EmialTaskData.Models;

namespace EmailTaskApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class MailController : Controller
    {
        private readonly IMailService _mailService;
        private readonly IMapper _mapper;

        public MailController(IMailService mailService, IMapper mapper)
        {
            _mailService = mailService;
            _mapper = mapper;
        }

        /// <summary>
        /// Add Emial to group
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /addMailToGroup
        ///     {
        ///        "groupId": 7,
        ///         "emialId": 1,
        ///          "userId": 0
        ///     }
        ///
        /// </remarks>
        /// <param name="emailGroupSubscription"></param>
        /// <response code="204">Returns when emial added to group successfully</response>
        /// <response code="404">If emial or group not found</response>
        /// <response code="400">If any unhandled exception</response>
        /// <response code="401">If user is not authorized</response>
        [HttpPost("addMailToGroup")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public ActionResult AddMailToGroup([FromBody] EmailGroupSubscription emailGroupSubscription)
        {
            try
            {
                _mailService.AddMailToGroup(emailGroupSubscription);
                return NoContent();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        /// <summary>
        /// Get all Emial for logged user.
        /// </summary>
        /// <returns>List of all Mail by user</returns>
        /// <response code="200">Return all Emial for current logged user</response>
        /// <response code="400">If any unhandled exception</response>
        /// <response code="401">If user is not authorized</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public ActionResult<IEnumerable<MailGroupModel>> Get()
        {
            try
            {
                return Ok(_mailService.Get());
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Get Emial for logged user by id.
        /// </summary>
        /// <returns>Emial</returns>
        /// <response code="200">Returns Emial by id for logged user</response>
        /// <response code="404">If Emial not founded</response>
        /// <response code="400">If any unhandled exception</response>
        /// <response code="401">If user is not authorized</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public ActionResult<MailGroupModel> Get(int id)
        {
            try
            {
                var mail = _mailService.Get(id);
                return Ok(mail);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }


        /// <summary>
        /// Creates a Emial.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Create
        ///     {
        ///        "emial": "emial@gmial.com"
        ///     }
        ///
        /// </remarks>
        /// <param name="newMail"></param>
        /// <returns>A newly created Emial</returns>
        /// <response code="201">Returns the newly created emial</response>
        /// <response code="400">If the emial is null</response>
        [HttpPost("createMail")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<MailModel> Post([FromBody] NewMail newMail)
        {
            try
            {
                var mailModel = _mapper.Map<Mail>(newMail);
                var createMail = _mailService.Create(mailModel);

                return CreatedAtAction(nameof(Get), new { id = createMail.Id }, createMail);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        /// <summary>
        /// Update a mail by id from emial list added by user
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT
        ///     {   "id":       1,
        ///         "emial":     "test",
        ///         "userId":   2,
        ///     }
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="mailModel"></param>
        /// <returns>New Created Group with group id and creator id</returns>
        /// <response code="201">Return the new created mail group with user id</response>
        /// <response code="400">If any unhandled exception</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<MailGroupModel> Put(int id, [FromBody] MailModel mailModel)
        {
            if (id != mailModel.Id)
            {
                return BadRequest();
            }

            try
            {
                var updatedModel = _mailService.Update(mailModel);
                
                if (updatedModel is null)
                {
                    return NotFound();
                }

                return NoContent();
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }


        /// <summary>
        /// Deletes a specific Emial from list added by user.
        /// </summary>
        /// <param name="id"></param>
        /// <response code="204">If group deleted</response>
        /// <response code="404">If group for delete not founded</response>
        /// <response code="400">If any unhandled exception</response> 
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Delete(int id)
        {
            try
            {
                _mailService.Delete(id);
                return NoContent();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
