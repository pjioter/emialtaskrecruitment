﻿using AutoMapper;
using EmialTaskBll.Service.MailGroupService.Dto;
using EmialTaskBll.Service.MailService.Dto;
using EmialTaskBll.Service.UserService.Dto;
using EmialTaskData.Models;

namespace EmailTaskApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<RegisterModel, User>();
            CreateMap<MailGroupModel, MailGroup>();
            CreateMap<NewMailGroupModel, MailGroup>();
            CreateMap<NewMail, Mail>();
        }
    }

}
