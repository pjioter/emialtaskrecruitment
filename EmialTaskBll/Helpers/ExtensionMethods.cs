﻿using EmialTaskData.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EmialTaskBll.Helpers
{
    public static class ExtensionMethods
    {
        public static IEnumerable<User> WithoutPasswords(this IEnumerable<User> users)
        {
            return users.Select(x => x.WithoutPassword());
        }

        public static User WithoutPassword(this User user)
        {
            user.PasswordHash = null;
            user.PasswordSalt = null;
            return user;
        }

        public static int CurrentLogedUserId(this IHttpContextAccessor httpContextAccessor)
        {
            int userId;
            if(int.TryParse(httpContextAccessor.HttpContext.User.Claims.ToList()[0].Value, out userId))
            {
                return userId;
            }
            throw new Exception();
        }
    }
}
