﻿using System.Collections.Generic;
using EmialTaskBll.Service.MailService.Dto;
using EmialTaskData.Models;

namespace EmialTaskBll.Service.MailService
{
    public interface IMailService
    {
        IEnumerable<MailModel> Get();

        MailModel Get(int id);

        MailModel Create(Mail mail);

        MailModel Update(MailModel mailGroupModel);

        void Delete(int id);
        void AddMailToGroup(EmailGroupSubscription emailGroupSubscription);
    }
}
