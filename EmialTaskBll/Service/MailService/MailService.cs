﻿using System.Collections.Generic;
using System.Linq;
using EmialTaskBll.CustomException;
using EmialTaskBll.Helpers;
using EmialTaskBll.Service.MailService.Dto;
using EmialTaskData.Models;
using EmialTaskData.Utilities;
using Microsoft.AspNetCore.Http;

namespace EmialTaskBll.Service.MailService
{
    public class MailService: IMailService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public MailService(UnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor)
        {
            _unitOfWork = unitOfWork;
            _httpContextAccessor = httpContextAccessor;
        }

        public IEnumerable<MailModel> Get()
        {
            var mailByUser = _unitOfWork.GetRepository<Mail>().GetList()
                .Where(x => x.UserId == _httpContextAccessor.CurrentLogedUserId())
                .Select(v => new MailModel()
                {
                    Id = v.Id,
                    Emial = v.Emial,
                    UserId = v.UserId,
                });
            return mailByUser;
        }

        public MailModel Get(int id)
        {
            var mail = _unitOfWork.GetRepository<Mail>().GetList()
                .FirstOrDefault(x => x.UserId == _httpContextAccessor.CurrentLogedUserId() && x.Id == id);

            if (mail is null)
            {
                throw new NotFoundException();
            }

            return new MailModel()
            {
                Emial = mail.Emial,
                Id = mail.Id,
                UserId = mail.UserId,
            };
        }

        public MailModel Create(Mail mail)
        {
            mail.UserId = _httpContextAccessor.CurrentLogedUserId();
            _unitOfWork.GetRepository<Mail>().Insert(mail);
            _unitOfWork.Save();

            return new MailModel()
            {
                Id = mail.Id,
                Emial = mail.Emial,
                UserId = mail.UserId,
            };

        }

        public MailModel Update(MailModel mailGroupModel)
        {
            var mailDbmodel = _unitOfWork.GetRepository<Mail>().GetByID(mailGroupModel.Id);

            if (mailDbmodel is null || mailDbmodel.UserId != _httpContextAccessor.CurrentLogedUserId())
            {
                return null;
            }

            mailDbmodel.Emial = mailGroupModel.Emial;
            _unitOfWork.Save();

            return new MailModel()
            {
                Id = mailDbmodel.Id,
                UserId = mailDbmodel.UserId,
                Emial = mailDbmodel.Emial,
            };
        }

        public void Delete(int id)
        {
            var mail = _unitOfWork.GetRepository<Mail>().GetByID(id);
            if (mail is null)
            {
                throw new NotFoundException();
            }

            _unitOfWork.GetRepository<Mail>().Delete(mail);
        }

        public void AddMailToGroup(EmailGroupSubscription emailGroupSubscription)
        {
            var mialGroup = _unitOfWork.GetRepository<MailGroup>().GetByID(emailGroupSubscription.GroupId);
            
            if (mialGroup is null || mialGroup.UserId != _httpContextAccessor.CurrentLogedUserId())
            {
                throw new NotFoundException();
            }

            var mail = _unitOfWork.GetRepository<Mail>().GetByID(emailGroupSubscription.EmialId);
            
            if (mail is null || mail.UserId != _httpContextAccessor.CurrentLogedUserId())
            {
                throw new NotFoundException();
            }

            mail.MailGroupId = emailGroupSubscription.GroupId;

            _unitOfWork.Save();
        }

    }
}
