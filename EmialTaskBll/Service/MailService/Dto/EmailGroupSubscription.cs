﻿namespace EmialTaskBll.Service.MailService.Dto
{
    public class EmailGroupSubscription
    {
        public int GroupId { get; set; }

        public int EmialId { get; set; }

        public int UserId { get; set; }
    }
}