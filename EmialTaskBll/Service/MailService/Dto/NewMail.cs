﻿using System.ComponentModel.DataAnnotations;
using EmialTaskData.Const;

namespace EmialTaskBll.Service.MailService.Dto
{
    public class NewMail
    {
        [StringLength(450)]
        [Required]
        [RegularExpression(RegularExpression.EmialRegex)]
        public string Emial { get; set; }
    }
}