﻿using EmialTaskData.Models;
using System.Threading.Tasks;


namespace EmialTaskBll.Service.UserService
{
    public interface IUserService
    {
        Task<User> Authenticate(string username, string password);

        User Create(User user, string password);

    }
}
