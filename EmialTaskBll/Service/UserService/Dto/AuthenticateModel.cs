﻿using System.ComponentModel.DataAnnotations;

namespace EmialTaskBll.Service.UserService.Dto
{
    public class AuthenticateModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
