﻿using System.Collections.Generic;
using System.Linq;
using EmialTaskBll.CustomException;
using EmialTaskBll.Helpers;
using EmialTaskBll.Service.MailGroupService.Dto;
using EmialTaskBll.Service.MailService.Dto;
using EmialTaskData.Models;
using EmialTaskData.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace EmialTaskBll.Service.MailGroupService
{
    public class MailGroupService: IMailGroupService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public MailGroupService(UnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor)
        {
            _unitOfWork = unitOfWork;
            _httpContextAccessor = httpContextAccessor;
        }

        public IEnumerable<MailGroupModel> Get()
        {
            var mailGroupsbyUser = _unitOfWork.GetRepository<MailGroup>().GetList()
                .Where(x => x.UserId == _httpContextAccessor.CurrentLogedUserId())
                .Select(v => new MailGroupModel()
                {
                    Id = v.Id,
                    GroupName = v.GroupName,
                    UserId = v.UserId,
                });
            return mailGroupsbyUser;
        }
        
        public MailGroupModel Get(int id)
        {
            var mailGroup = _unitOfWork.GetRepository<MailGroup>().GetList()
                .FirstOrDefault(x => x.UserId == _httpContextAccessor.CurrentLogedUserId() && x.Id == id);
            
            if (mailGroup is null)
            {
                throw new NotFoundException();
            }

            return new MailGroupModel()
            {
                GroupName = mailGroup.GroupName,
                Id = mailGroup.Id,
                UserId = mailGroup.UserId,
            };
        }

        public GroupWithMails GetAllEmialFromGroup(int id)
        {
            var mailGroup = _unitOfWork.GetRepository<MailGroup>().GetList().Include(o => o.MailNavigation)
                .FirstOrDefault(x => x.UserId == _httpContextAccessor.CurrentLogedUserId() && x.Id == id);
            if (mailGroup is null)
            {
                throw new NotFoundException();
            }

            return new GroupWithMails()
            {
                MailGroupModel =
                    new MailGroupModel()
                    {
                        GroupName = mailGroup.GroupName, Id = mailGroup.Id, UserId = mailGroup.UserId,
                    },
                MailModel = mailGroup.MailNavigation.Select(v => new MailModel()
                {
                    Emial = v.Emial, Id = v.Id, UserId = mailGroup.UserId
                })
            };
        }

        public MailGroupModel Create(MailGroup mailGroup)
        {
            mailGroup.UserId = _httpContextAccessor.CurrentLogedUserId();
            _unitOfWork.GetRepository<MailGroup>().Insert(mailGroup);
            _unitOfWork.Save();

            return new MailGroupModel()
            {
                Id = mailGroup.Id,
                GroupName = mailGroup.GroupName,
                UserId = mailGroup.UserId,
            };

        }

        public MailGroupModel Update(MailGroupModel mailGroupModel)
        {
            var mailGroupDbItem = _unitOfWork.GetRepository<MailGroup>().GetByID(mailGroupModel.Id);
            
            if (mailGroupDbItem is null || mailGroupDbItem.UserId != _httpContextAccessor.CurrentLogedUserId())
            {
                return null;
            }

            mailGroupDbItem.GroupName = mailGroupModel.GroupName;
            _unitOfWork.Save();

            return new MailGroupModel()
            {
                Id = mailGroupDbItem.Id,
                UserId = mailGroupDbItem.UserId,
                GroupName = mailGroupDbItem.GroupName,
            };
        }

        public void Delete(int groupId)
        {
            var groupForDelete = _unitOfWork.GetRepository<MailGroup>().GetByID(groupId);
            if (groupForDelete is null)
            {
                throw new NotFoundException();
            }

            var list = _unitOfWork.GetRepository<Mail>().GetList().Where(x => x.MailGroupId == groupForDelete.Id);

            foreach (var mail in list)
            {
                mail.MailGroupId = null;
            }

            _unitOfWork.GetRepository<MailGroup>().Delete(groupForDelete);
            _unitOfWork.Save();
        }


    }
}
