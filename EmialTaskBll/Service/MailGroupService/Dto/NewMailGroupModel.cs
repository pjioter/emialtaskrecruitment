﻿using System.ComponentModel.DataAnnotations;

namespace EmialTaskBll.Service.MailGroupService.Dto
{
    public class NewMailGroupModel
    {
        [Required]
        public string GroupName { get; set; }
    }
}