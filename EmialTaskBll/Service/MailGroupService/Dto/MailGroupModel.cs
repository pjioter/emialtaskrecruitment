﻿using System.ComponentModel.DataAnnotations;

namespace EmialTaskBll.Service.MailGroupService.Dto
{
    public class MailGroupModel
    {
        public int Id { get; set; }

        [Required]
        public string GroupName { get; set; }

        public int UserId { get; set; }
    }
}
