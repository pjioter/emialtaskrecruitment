﻿using System.Collections.Generic;
using EmialTaskBll.Service.MailService.Dto;

namespace EmialTaskBll.Service.MailGroupService.Dto
{
    public class GroupWithMails
    {
        public MailGroupModel MailGroupModel { get; set; }

        public IEnumerable<MailModel> MailModel { get; set; }
    }
}