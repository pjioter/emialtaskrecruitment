﻿using System.Collections.Generic;
using EmialTaskBll.Service.MailGroupService.Dto;
using EmialTaskData.Models;

namespace EmialTaskBll.Service.MailGroupService
{
    public interface IMailGroupService
    {
        IEnumerable<MailGroupModel> Get();

        MailGroupModel Create(MailGroup mailGroup);

        MailGroupModel Get(int id);

        void Delete(int groupId);

        MailGroupModel Update(MailGroupModel mailGroupModel);

        GroupWithMails GetAllEmialFromGroup(int id);
    }
}
