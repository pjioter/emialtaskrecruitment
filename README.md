Celem Twojego zadana, jest stworzenie API aplikacji do tworzenia grup mailingowych.
● Grupy można listować, dodawać, edytować i usuwać. Każda grupa powinna mieć swoją unikalną
nazwę.
● W aplikacji powinniśmy mieć możliwość dodawania do grupy adresów mailowych, które można
listować, dodawać, edytować oraz usuwać.
● Dane aplikacji powinny być przechowywane z dowolnej, relacyjnej bazie danych
● Aplikacja powinna być przetestowana i udokumentowana zgodnie ze standardem OAS
● API powinno być zabezpieczone
● Dostęp do aplikacji powinien odbywać się przez konto użytkownika, które można założyć
● Dane per konto są anonimowe tzn. inny użytkownik nie może widzieć danych drugiego
Tech-stack: .NET Core/C#
Udostępnienie zadania:
Rozwiązanie zadania umieść proszę na publicznie dostępnych serwisach do hostowania kodu - np. github,
bitbucket, itp. i prześlij nam link do repozytorium.
W zadaniu będziemy oceniać:
● jakość Twojego kodu,
● podział kodu na warstwy,
● znajomość składni języków i frameworków,
● zaproponowaną strukturę baz danych,
● umiejętność pisania testów jednostkowych
● wykorzystanie IoC
● użycie wzorców projektowych